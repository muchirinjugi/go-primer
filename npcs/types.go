package npcs

type Power struct {
	Attack, Defense int
}

type Location struct {
	X, Y, Z float64
}

// NonPlayerCharacter it a struct
type NonPlayerCharacter struct {
	Name      string
	Speed, HP int
	Power,
	Loc Location
}
