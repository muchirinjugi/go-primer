// package main

// import (
// 	"fmt"
// 	"math"
// 	"os"

// 	"github.com/olekukonko/tablewriter"
// )

// type power struct {
// 	attack, defense int
// }

// type location struct {
// 	x, y, z float64
// }

// type nonPlayerCharacter struct {
// 	name      string
// 	speed, hp int
// 	power     power
// 	loc       location
// }

// func (loc location) String() string {
// 	return fmt.Sprintf("(%f,%f,%f)", loc.x, loc.y, loc.z)
// }

// // calculate distance between locations
// func (loc location) euclideanDistance(target location) float64 {
// 	distanceBetweenXcordinates := (loc.x - target.x) * 2
// 	distanceBetweenYcordinates := (loc.y - target.y) * 2
// 	distanceBetweenZcordinates := (loc.z - target.z) * 2
// 	total := distanceBetweenXcordinates + distanceBetweenYcordinates + distanceBetweenZcordinates

// 	return math.Sqrt(total)
// }

// func (npc nonPlayerCharacter) distanceTo(target nonPlayerCharacter) float64 {
// 	return npc.loc.euclideanDistance(target.loc)
// }

// func createCharacter() (nonPlayerCharacter, nonPlayerCharacter) {
// 	fmt.Println("Structs....")
// 	demon := nonPlayerCharacter{
// 		name:  "Alfred",
// 		speed: 21,
// 		hp:    1000,
// 		power: power{
// 			attack:  75,
// 			defense: 50,
// 		},
// 		loc: location{
// 			x: 1075.123,
// 			y: 521.123,
// 			z: 211.231,
// 		},
// 	}

// 	anotherDemon := nonPlayerCharacter{
// 		name:  "Beelzebub",
// 		speed: 30,
// 		hp:    5000,
// 		power: power{
// 			attack:  10,
// 			defense: 10,
// 		},
// 		loc: location{
// 			x: 32.03,
// 			y: 72.45,
// 			z: 65.231,
// 		},
// 	}

// 	return demon, anotherDemon
// }

// // two

// type attacker struct {
// 	attackPower, dmgbonus int
// }

// type sword struct {
// 	attacker
// 	twoHanded bool
// }

// type gun struct {
// 	attacker
// 	bulletsRemaining int
// }

// func (s sword) wield() bool {
// 	fmt.Println("You've wielded a sword")
// 	return true
// }
// func (g gun) wield() bool {
// 	fmt.Println("You've wielded a gun! ")
// 	return true
// }

// type weapon interface {
// 	wield() bool
// }

// func wielder(w weapon) bool {
// 	fmt.Println("Wielding...")
// 	return w.wield()
// }

// func createWeapon() {
// 	swordOne := sword{
// 		attacker: attacker{
// 			attackPower: 1,
// 			dmgbonus:    5,
// 		},
// 		twoHanded: true,
// 	}
// 	gunOne := gun{
// 		attacker: attacker{
// 			attackPower: 10,
// 			dmgbonus:    20,
// 		},
// 		bulletsRemaining: 11,
// 	}

// 	wielder(swordOne)
// 	wielder(gunOne)
// }

// func main() {
// 	demonOne, demonTwo := createCharacter()

// 	data := [][]string{
// 		[]string{"Alfred", "15", "10/20", "(10.32, 56.21, 30.25)"},
// 		[]string{"Beelzebub", "30", "30/50", "(1, 1, 1)"},
// 		[]string{"Hortense", "30", "80/80", "(1, 1, 1)"},
// 		[]string{"Pokey", "8", "30/40", "(1, 1, 1)"},
// 	}

// 	table := tablewriter.NewWriter(os.Stdout)
// 	table.SetHeader([]string{"NPC", "Speed", "Power", "Location"})
// 	table.AppendBulk(data)
// 	table.Render()

// 	fmt.Printf("Npc %v is %f units away from Npc %v\n", demonOne, demonOne.distanceTo(demonTwo), demonTwo)

// 	createWeapon()
// }
